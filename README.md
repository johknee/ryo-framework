# Things to do to launch the site #

Uploading the site should redirect you to the install page where a lot of the set up is made, however there is some scss stuff to setup for the design

## Column counts and widths ##

In [_grid.scss](https://bitbucket.org/johknee/ryo-framework/src/080e5e03de44b4b5ff689e24a3d17210e3d90d9f/sass/_grid.scss?at=master) you can set the width, the gutter and the column count then on compile it will create everything you need

## Media query breakpoints ##

In [style.scss](https://bitbucket.org/johknee/ryo-framework/src/080e5e03de44b4b5ff689e24a3d17210e3d90d9f/sass/style.scss?at=master) and [libs/_helpers.scss](https://bitbucket.org/johknee/ryo-framework/src/080e5e03de44b4b5ff689e24a3d17210e3d90d9f/sass/libs/_helpers.scss?at=master) you can change the breakpoints

### Terminal commands ###

These are some terminal commands that are worth remembering for this.

This need to be executed in the root of your project and will compile sass on the fly  
`sudo gem install sass`  
`sass --watch sass:dev/css`

This can be executed anywhere but then the following one needs to executed inside the sass folder  
`sudo gem install bourbon`  
`cd sass`  
`bourbon install`

This is for the composer elements

`curl -s https://getcomposer.org/installer | php`  
`php composer.phar install`

Install gulp through node

`sudo npm install gulp`

and because of the gulpfile.js, just run this

`gulp`