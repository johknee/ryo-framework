<?php

function url(){
	$url = sprintf(
		"%s://%s%s",
		isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
		$_SERVER['SERVER_NAME'],
		$_SERVER['REQUEST_URI']
	);
	
	return str_replace("/install.php","",$url);
}

if($_POST)
{
	if(file_exists("settings.json"))
	{
		unlink("settings.json");
	}
	
	$dbObj = array(
		"host"		=> $_POST['site_database_host'],
		"username"	=> $_POST['site_database_username'],
		"password"	=> $_POST['site_database_password'],
		"db"		=> $_POST['site_database_name']
	);
	
	$dbObj = (object) $dbObj;
	
	$arr = array(
		"title"				=> $_POST['site_title'],
		"database"			=> $_POST['site_database'],
		"database_details"	=> $dbObj,
		"site_url"			=> url(),
		"site_directory"	=> dirname(__FILE__)
	);
	
	$arr = (object) $arr;
	
	$db_host 		= $arr->database_details->host;
	$db_username	= $arr->database_details->username;
	$db_password	= $arr->database_details->password;
	$db_name		= $arr->database_details->db;
	
	$connect = mysql_connect($db_host,$db_username,$db_password) or die("Could not connect with details: ".mysql_error());
	mysql_select_db($db_name) or die("Could not connect to database");
	
	$fp = fopen('settings.json', 'w');
	fwrite($fp, json_encode($arr));
	fclose($fp);
	chmod('settings.json', 0600);
	
	header("location:".$arr->site_url);
}

?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Install</title>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <link href="assets/css/install.css" media="screen, projection" rel="stylesheet" type="text/css" />
</head>

<body>

	<form action="" id="install_form" method="post">
        <div class="row container">
        	<div class="block-12">
            	<h1>Install the site</h1>
            </div>
            <div class="block-3">
                <label for="site_title">Site title</label>
            </div>
            <div class="block-9">
                <input id="site_title" type="text" name="site_title" placeholder="Site title"/>
            </div>
        </div>
        <div class="row container">
            <div class="block-3">
                <label for="site_database">Need a database?</label>
            </div>
            <div class="block-9">
            	<span class="check">
                	<input type="radio" name="site_database" class="site_database" value="yes" checked/>
                    Yes
                </span>
            	<span class="check">
                	<input type="radio" name="site_database" class="site_database" value="no"/>
                    No
                </span>
            </div>
        </div>
        <div class="row container database_rows">
            <div class="block-3">
                <label for="site_database_host">Database host</label>
            </div>
            <div class="block-9">
                <input id="site_database_host" type="text" name="site_database_host" placeholder="Database host"/>
            </div>
        </div>
        <div class="row container database_rows">
            <div class="block-3">
                <label for="site_database_username">Database username</label>
            </div>
            <div class="block-9">
                <input id="site_database_username" type="text" name="site_database_username" placeholder="Database username"/>
            </div>
        </div>
        <div class="row container database_rows">
            <div class="block-3">
                <label for="site_database_password">Database password</label>
            </div>
            <div class="block-9">
                <input id="site_database_password" type="password" name="site_database_password" placeholder="Database password"/>
            </div>
        </div>
        <div class="row container database_rows">
            <div class="block-3">
                <label for="site_database_name">Database name</label>
            </div>
            <div class="block-9">
                <input id="site_database_name" type="text" name="site_database_name" placeholder="Database name"/>
            </div>
        </div>
        <div class="row container">
            <div class="block-3">
            </div>
            <div class="block-9">
            	<input type="submit" value="Configure site"/>
            </div>
        </div>
    </form>
    
    <script>
	$(".site_database").change(function(e) {
		var currVal = $(".site_database:checked").val();
		
		if(currVal=="yes")
		{
			$(".database_rows").show();
		}
		else
		{
			$(".database_rows").hide();
		}
    });
	</script>

</body>
</html>