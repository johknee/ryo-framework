<?php
class jtRouter
{
	public function serve($url,$arr)
	{
		if($url=="")
		{
			$parts = array(
				"total_levels" => 0,
				"base" => "Home",
				"variable" => "",
				"page" => 1
			);
			$parts = (object) $parts;
			TemplateHandler::head($parts);
			$arr['/']::make();
			TemplateHandler::foot($parts);
		}
		else
		{
			$parts = $this->splitter($url);
			
			if(isset($arr[$parts->base]))
			{
				TemplateHandler::head($parts);
				$arr[$parts->base]::make($parts->variable,$parts->page);
				TemplateHandler::foot($parts);
			}
			else
			{
				$parts = array(
					"total_levels" => 0,
					"base" => "Error",
					"variable" => "404 Page not found",
					"page" => 1
				);
				$parts = (object) $parts;
				
				TemplateHandler::head($parts);
				ErrorHandler::make("404",$parts);
				TemplateHandler::foot($parts);
			}
		}
	}
	
	private function splitter($part)
	{
		$exp = explode("/",$part);
		
		if(end($exp)=="")
		{
			array_pop($exp);
		}
		
		$total = count($exp);
		
		if($total>1)
		{
			$var = $exp[1];
		}
		
		$page = 1;
		
		$key = array_search('page', $exp);
		
		if($key>0)
		{
			$page = $exp[$key+1];
		}
		
		$return = array(
			"total_levels" => $total,
			"base" => $exp[0],
			"variable" => $var,
			"page" => $page
		);
		
		return (object) $return;
	}
}
?>