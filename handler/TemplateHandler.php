<?php

class TemplateHandler
{
	public function head($parts) {
		global $settings;
		include $settings->site_directory."/includes/meta_head.php";
		include $settings->site_directory."/views/template/header.php";
	}
	public function foot($parts) {
		global $settings;
		include $settings->site_directory."/views/template/footer.php";
	}
}

?>