var gulp = require('gulp'); 

var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var newer = require('gulp-newer');
var image = require('gulp-image');

gulp.task('imagemin', function() {
  var imgSrc = './dev/images/**/*',
      imgDst = './assets/images';
 
  gulp.src(imgSrc)
    .pipe(newer(imgDst))
    .pipe(image())
    .pipe(gulp.dest(imgDst));
});

gulp.task('styles', function() {
  gulp.src(['./dev/css/*.css'])
    .pipe(autoprefix("last 1 version", "> 1%", "ie 8", "ie 7"))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./assets/css/'));
});

gulp.task('scripts', function() {
  gulp.src(['./dev/js/*.js'])
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('./assets/js/'));
});



gulp.task('default', ['imagemin', 'scripts', 'styles'], function() {
	gulp.watch('./dev/js/*.js', function() {
		gulp.run('jshint', 'scripts');
	});
	
	gulp.watch('./dev/css/*.css', function() {
		gulp.run('styles');
	});
	
	gulp.watch('./dev/images/*', function() {
		gulp.run('imagemin');
	});
});