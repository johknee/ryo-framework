<?php

if(file_exists($root."settings.json"))
{
	$settings = file_get_contents($root."settings.json");
	$settings = json_decode($settings);
}
else
{
	header("location:".$root."install.php");
}

require_once $settings->site_directory."/vendor/autoload.php";
require_once $settings->site_directory."/handler/bootstrap.php";

if($settings->database=="yes")
{
	require_once $settings->site_directory."/includes/config.php";
	require_once $settings->site_directory."/includes/setup.php";
}

require_once $settings->site_directory."/includes/functions.php";

$router = new jtRouter();

$router->serve($_GET['url'],array(
	"test" 	=> "TestHandler",
	"/"		=> "MainHandler"
));

?>