<!doctype html>
<html>
<head>
    <!-- - - META TAGS - - -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php
    $title = $parts->variable;
	if(!$title)
	{
		$title = $parts->base;
	}
	echo createTitle(ucfirst($title),"|",$settings->title);?></title>
    <!-- - - END META TAGS - - -->
    
    <!-- - - SCRIPTS - - -->
    <script src="<?php echo $settings->site_url;?>/assets/js/modernizr.js"></script>
    <script>
	Modernizr.load({
		load: [
			'https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js',
			'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js'
		],
		complete: function () {
			Modernizr.load({
				load: [
					'<?php echo $settings->site_url;?>/assets/js/library.js',
					'<?php echo $settings->site_url;?>/assets/js/scripts.js'
				],
				complete : function(){
					init();
				}
			});
		}
	});
	</script>
    <!-- - - END SCRIPTS - - -->
    
    <!-- - - STYLESHEETS - - -->
    <link href="<?php echo $settings->site_url;?>/assets/css/style.css" media="screen, projection" rel="stylesheet" type="text/css" />
    <link href="<?php echo $settings->site_url;?>/assets/css/print.css" media="print" rel="stylesheet" type="text/css" />
    <!--[if IE]>
    	<link href="<?php echo $settings->site_url;?>/assets/css/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!-- - - END STYLESHEETS - - -->
</head>

<body>